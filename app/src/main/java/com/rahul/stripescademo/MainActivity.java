package com.rahul.stripescademo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardMultilineWidget;
import com.stripe.android.view.PaymentMethodsActivity;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Stripe mStripe;

    private SmoothProgressBar progress;
    private CardMultilineWidget cardMultilineWidget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = findViewById(R.id.progress);
        cardMultilineWidget = findViewById(R.id.cardMultilineWidget);

        mStripe = new Stripe(this, PaymentConfiguration.getInstance().getPublishableKey());

        CustomerSession.initCustomerSession(this, new AppEphemeralKeyProvider(this));
    }

    public void onContinueClicked(View view) {
        Card card = cardMultilineWidget.getCard();
        if (card == null) {
            Toast.makeText(this, "Enter card details and try again.", Toast.LENGTH_SHORT).show();
            return;
        }

        fetchClientSecret(card);
    }

    private void fetchClientSecret(final Card card) {
        progress.setVisibility(View.VISIBLE);

        Ion.with(this)
                .load("http://192.168.0.103:5000/stripeClientSecret")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject jsonObject) {
                        Log.d(TAG, "onCompleted: " + new Gson().toJson(jsonObject));
                        mStripe.confirmPayment(MainActivity.this, createConfirmPaymentIntentParams(card, jsonObject.get("client_secret").getAsString()));
                    }
                });
    }

    private ConfirmPaymentIntentParams createConfirmPaymentIntentParams(Card card, String clientSecret) {
        final PaymentMethodCreateParams paymentMethodCreateParams = PaymentMethodCreateParams.create(card.toPaymentMethodParamsCard(), null);
        return ConfirmPaymentIntentParams.createWithPaymentMethodCreateParams(paymentMethodCreateParams, clientSecret, "https://www.google.com");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mStripe.onPaymentResult(requestCode, data, new ApiResultCallback<PaymentIntentResult>() {
            @Override
            public void onSuccess(@NonNull PaymentIntentResult result) {
                Log.d(TAG, "onSuccess: " + new Gson().toJson(result));
                progress.setVisibility(View.GONE);
                final PaymentIntent paymentIntent = result.getIntent();
                final PaymentIntent.Status status = paymentIntent.getStatus();
                if (status == PaymentIntent.Status.Succeeded) {
                    Toast.makeText(MainActivity.this, "Payment successful", Toast.LENGTH_LONG).show();
                } else if (PaymentIntent.Status.RequiresPaymentMethod == status) {
                    Toast.makeText(MainActivity.this, "Authentication failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(@NonNull Exception e) {
                progress.setVisibility(View.GONE);
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Error while making payment.", Toast.LENGTH_LONG).show();
            }
        });

        if (requestCode == 200) {
            PaymentMethod paymentMethod = data.getParcelableExtra(PaymentMethodsActivity.EXTRA_SELECTED_PAYMENT);
            if (paymentMethod == null) {
                Toast.makeText(this, "Transaction failed", Toast.LENGTH_SHORT).show();
                return;
            }
            fetchClientSecretById(paymentMethod.id);
        }
    }

    private void fetchClientSecretById(final String paymentMethodId) {
        progress.setVisibility(View.VISIBLE);

        Ion.with(this)
                .load("http://192.168.0.103:5000/stripeLoggedInClientSecret")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject jsonObject) {
                        Log.d(TAG, "onCompleted: " + new Gson().toJson(jsonObject));
                        mStripe.confirmPayment(MainActivity.this, createConfirmPaymentIntentParams(jsonObject.get("client_secret").getAsString(), paymentMethodId));
                    }
                });
    }

    @NonNull
    private ConfirmPaymentIntentParams createConfirmPaymentIntentParams(@NonNull String clientSecret, @NonNull String paymentMethodId) {
        return ConfirmPaymentIntentParams.createWithPaymentMethodId(paymentMethodId, clientSecret, "https://www.google.com");
    }

    public void onPayUsingSavedCardClicked(View view) {
        startActivityForResult(new Intent(this, PaymentMethodsActivity.class), 200);
    }
}
