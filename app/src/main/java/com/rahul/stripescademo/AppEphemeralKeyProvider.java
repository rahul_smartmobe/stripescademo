package com.rahul.stripescademo;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.stripe.android.EphemeralKeyProvider;
import com.stripe.android.EphemeralKeyUpdateListener;

public class AppEphemeralKeyProvider implements EphemeralKeyProvider {
    private Context context;

    public AppEphemeralKeyProvider(Context context) {
        this.context = context;
    }

    @Override
    public void createEphemeralKey(@NonNull String apiVersion, @NonNull final EphemeralKeyUpdateListener keyUpdateListener) {
        Ion.with(context)
                .load("https://qa-ba4.izy.as/api/stripe/ephemeral-key")
                .addHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVhMGUzZDM5NjMxN2ZiZDMyYWFiZjY4YTc2ZmEyZTA1NDYxNDU2NmYzYjZlY2I0MTdlMmY4ZWRkMGZkNjA1OTNhMzg4ZjU4Yjk1MTE1ZDExIn0.eyJhdWQiOiIyIiwianRpIjoiZWEwZTNkMzk2MzE3ZmJkMzJhYWJmNjhhNzZmYTJlMDU0NjE0NTY2ZjNiNmVjYjQxN2UyZjhlZGQwZmQ2MDU5M2EzODhmNThiOTUxMTVkMTEiLCJpYXQiOjE1NjU3NjA5OTgsIm5iZiI6MTU2NTc2MDk5OCwiZXhwIjoxNTY1NzY4MTk4LCJzdWIiOiIzMDAiLCJzY29wZXMiOltdfQ.EH1fvK33anadmhROSOjp72yoqQR3DBp-8UhUhvH4g7WW2HbCI1Bp7wnixqwFO80Vv7t-R_8ltxNHEm7kbrU5aM1AehLjF01REBpFwfTSQrU-JmpQ0gIibhRKRlFSWqWmeUQFnqxBj9RfPVa2i7jLbhEecqumSkG5xldRJl9GLZcWYleygQDTowVQxRHhkSUWzB5k3jzn_hS9PgoBVOqnynKF2n7cYAcbXOb_jFh82Cz6uAWfKoqrtmPRzMJDceF205JmSozOvXewTsdCGI_qPoKzGr2V1k30MMSKslrMRQyiAdEeXqRIO05rm9t_Hb6FxLZnSKYviCvDrqJh0IvcGCOJGW_6YrTqtnpIeAfi0VO7nv3JylSVcPTMghe9k-mLw16k4yQRWYJ1yOYQZUk1eoVPGmcSdG2KABVYf1Ff5dOEAopDbYQL2n0QdBGT45Rq5foitq7oDY5wzzUgM1yJzlETgppdQantY20LxUCTSMxURx1_mihSdotsVn5cdTsDn-2v-6gVZivB1p_JtxZLPASGCgEeIUcZWHlrqmITNia7s-nonLS76vsUD22_4pCphwtsht3-7OsfueMVCg3SmuJwgT9GyjkfQVS8qR8SdYBAisdZCH1OL2UqwFpMDliot3-bs5Og2KnwhM4u2Rjm1uAah01CxlXF4ee8d8Dky2E")
                .addQuery("api_version", apiVersion)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject jsonObject) {
                        Log.d("onCompleted: ", new Gson().toJson(jsonObject));
                        if (e == null)
                            keyUpdateListener.onKeyUpdate(new Gson().toJson(jsonObject.getAsJsonObject("body")));
                    }
                });

    }
}
